<?php

/**
 * Plugin Name:       Toy Head
 * Plugin URI:        https://gitlab.com/67656e65726963/toy-head
 * Description:       Helps tidy up HTML head.
 * Version:           0.0.0
 * Author:            67656e65726963
 * Author URI:        https://gitlab.com/67656e65726963
 * License:           Unlicense
 * License URI:       http://unlicense.org/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_action(
	'init',
	function() {
		global $wp_widget_factory;

		// This is recommended for added security
		remove_action( 'wp_head', 'wp_generator' );
		remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );

		// Windows Live Writer
		remove_action( 'wp_head', 'wlwmanifest_link' );
		remove_action( 'wp_head', 'rsd_link' );
		remove_action(
			'wp_head',
			array(
				$wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
				'recent_comments_style',
			)
		);

		// Oembed specific
		remove_action( 'wp_head', 'wp_oembed_add_host_js' );
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );

		// All actions/filters related to emojis
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );

		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

		flush_rewrite_rules();
	},
	67
);
